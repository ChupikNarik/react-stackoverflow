import React, { Component }  from 'react';
import { Provider } from 'react-redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createStore, applyMiddleware } from 'redux';

import { Router, Scene } from 'react-native-router-flux';

import ReduxThunk from 'redux-thunk';
import reducers from './src/reducers';

import Main from './src/pages';
import Page from './src/pages/viewPage';


const store = createStore(reducers, composeWithDevTools(
    applyMiddleware(ReduxThunk)
))


const App = (props) =>{
    return(
        <Provider store={store}>
            <Router>
                <Scene key="root">
                    <Scene key="main" component={Main} hideNavBar={true} initial/>
                    <Scene key="page" component={Page} hideNavBar={true}/>
                </Scene>
            </Router>
        </Provider>
    )
}

export default App
