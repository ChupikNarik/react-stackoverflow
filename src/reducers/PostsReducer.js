import { POSTS_CHANGE, CLEAR_STORE } from '../types';

const INITIAL_STATE = {
    postsList: []
}


export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case POSTS_CHANGE:
            var newData = state.postsList.concat(action.payload);
            return {
                ...state,
                postsList: newData
            }
        case CLEAR_STORE:
            return {
                ...state,
                postsList: []
            }
        default: return state
    }
}
