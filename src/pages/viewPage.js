import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TextInput, Dimensions, Image, WebView, ScrollView, TouchableOpacity} from 'react-native';

import Moment from 'react-moment';
import axios from 'react-native-axios';
import { Actions } from 'react-native-router-flux';

import constants from '../constants';

import styles from '../styles';
import { isIphoneX } from '../helpers/is-iphone-x';

class Page extends Component {
    constructor() {
        super();
        this.state = {
            loading: true,
            postData: [],
            userData: [],
            tags: []
        };
    }

    componentDidMount(){
        const HEADER_SIZE = isIphoneX() ? 130 : 100;
        if (HEADER_SIZE == 130) {
            this.setState({detectBrow: true});
        }
        axios.get(constants.API+'/'+this.props.postID+'?&site=stackoverflow&filter=withbody').then(
            (response) => {
                this.setState({postData: response.data.items[0]});
                this.setState({tags: response.data.items[0].tags});

                var userID = this.state.postData.owner.user_id;

                axios.get('https://api.stackexchange.com/2.2/users/'+userID+'?order=desc&sort=reputation&site=stackoverflow').then(
                    (responseUser) => {
                        this.setState({userData: responseUser.data.items[0]});
                        this.setState({loading: false});
                    }).catch(function (error) {
                        console.log(error);
                    });

            }).catch(function (error) {
                console.log(error);
            });
    }

    render() {
        if (this.state.loading == false) {
            return (
                <View style={this.state.detectBrow ? styles.containerX : styles.container}>
                    <View style={styles.singlePage}>
                        <TouchableOpacity style={styles.goBack} onPress={Actions.main}>
                            <Text style={styles.goBackText}>Go back</Text>
                        </TouchableOpacity>
                        <View style={styles.singleCard}>
                            <View style={styles.userNameImg}>
                                <View style={styles.userImg}>
                                    <Image
                                        style={{width: 50, height: 50}}
                                        source={{uri: 'https://www.gravatar.com/avatar/05b67e1712724c357c02bc97f7513948?s=128&d=identicon&r=PG&f=1'}}/>
                                </View>
                                <View style={styles.fillSpace}>
                                    <View style={styles.nameReputation}>
                                        <Text style={styles.name}>{this.state.userData.display_name}</Text>
                                        <Text style={styles.reputationDate}>{this.state.userData.reputation}</Text>
                                        <View style={styles.userReputation}>
                                            <Text style={styles.userReputationText}>{this.state.postData.view_count}</Text>
                                        </View>
                                    </View>
                                    <Moment unix style={styles.reputationDate} element={Text} fromNow>{this.state.userData.last_access_date}</Moment>
                                </View>
                            </View>
                        </View>
                        <WebView
                            source={{html: this.state.postData.body}}
                            style={{backgroundColor: '#ffffff', paddingLeft: 15, paddingRight: 15}}
                            />
                        <View style={{paddingLeft: 15, paddingRight: 15}}>
                            <ScrollView horizontal={true}>
                                <View style={styles.tagsList}>
                                    {this.state.tags.map((tag, index) =>
                                        <View style={styles.singleTag} key={index}>
                                            <Text style={{color: '#ffffff'}}>{tag}</Text>
                                        </View>
                                    )}
                                </View>
                            </ScrollView>
                        </View>
                    </View>
                </View>
            );
        }else{
            return (
                <View style={styles.container}>
                    <Text style={styles.loading}>Завантаження данних...</Text>
                </View>
            )
        }
    }
}

export default Page;
