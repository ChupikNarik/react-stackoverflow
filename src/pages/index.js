import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TextInput, Dimensions, Image, TouchableOpacity} from 'react-native';

import InfiniteScroll from 'react-native-infinite-scroll';

import axios from 'react-native-axios';
import Moment from 'react-moment';
import { Actions } from 'react-native-router-flux';
import Modal from "react-native-modal";
import DatePicker from 'react-native-datepicker';
import { Dropdown } from 'react-native-material-dropdown';
import moment from "moment";

import { connect } from 'react-redux';
import { postsChanged, clearStore } from '../actions';

import constants from '../constants';

import { isIphoneX } from '../helpers/is-iphone-x';

import styles from '../styles';


class App extends Component{
    constructor() {
        super();
        this.state = {
            data: null,
            loading: true,
            loadingData: false,
            startPage: 1,
            detectBrow: false,
            modalVisible: false,
            inputTags: '',
            todate: '',
            fromdate: '',
            order: '',
            sort: ''
        };
    }

    getPosts(param){
        console.log(constants.API+constants.DEFAULT_PARAMETRS+"&page="+param);
        axios.get(constants.API+constants.DEFAULT_PARAMETRS+"&page="+param).then(
            (response) => {
                var page = this.state.startPage;
                var nextPage = page + 1;

                this.setState({
                    loading: false,
                    startPage: nextPage
                });

                this.props.postsChanged(response.data.items);
                this.setState({loadingData: false});
            }).catch(function (error) {
                console.log(error);
            });
    }

    setModalVisability = (visible) => {
        this.setState({modalVisible: visible});
    }

    componentDidMount(){
        const HEADER_SIZE = isIphoneX() ? 130 : 100;
        if (HEADER_SIZE == 130) {
            this.setState({detectBrow: true});
        }
        this.getPosts(this.state.startPage);
    }

    loadMorePage = () => {
        if (this.state.loadingData == false) {
            this.getPosts(this.state.startPage);
        }
        this.setState({loadingData: true});
    }

    route = async (postID) =>{
        Actions.page({postID: postID});
    }
    search = () =>{
        var newParam = '';
        var tags = '';
        var todate = '';
        var fromdate = '';
        var order = '';
        var sort = '';
        if (this.state.inputTags != '') {
            tags = '&tagged='+this.state.inputTags;
        }else{
            tags = '';
        }

        if (this.state.todate != '') {
            todate = '&todate='+moment(this.state.todate, "YYYY-MM-DD").unix()
        }else{
            todate = '';
        }

        if (this.state.fromdate != '') {
            fromdate = '&fromdate='+moment(this.state.fromdate, "YYYY-MM-DD").unix();
        }else{
            fromdate = '';
        }

        if (this.state.order != '') {
            order = '&order='+this.state.order;
        }else{
            order = '';
        }

        if (this.state.sort != '') {
            sort = '&sort='+this.state.sort;
        }else{
            sort = '';
        }

        this.setState({
            loading: true,
            startPage: 1
        });

        this.setModalVisability(false);

        this.props.clearStore();
        this.getPosts(1+tags+todate+fromdate+order+sort);
    }
    validateInput = (text) =>{
        var validTags = text.replace(/\s/g, ',').replace(',,', ',');
        this.setState({inputTags: validTags})
    }
    showStore = () => {
        console.log(todate);
        console.log(fromdate);
    }
    render() {
        let order = [{
            value: 'desc',
        }, {
            value: 'asc',
        }];

        let sort = [{
            value: 'activity',
        }, {
            value: 'votes',
        }, {
            value: 'creation',
        }, {
            value: 'hot',
        }, {
            value: 'week',
        }, {
            value: 'month',
        }];

        if (this.state.loading == false) {
            return (
                <View style={this.state.detectBrow ? styles.containerX : styles.container}>
                    <View style={styles.search}>
                        <View style={styles.fieldHere}>
                            <TouchableOpacity style={styles.searchButton} onPress={() => this.search()}>
                                <Image source={require('../img/search-ico.png')} style={{width: 20, height: 20, marginTop: 10}}/>
                            </TouchableOpacity>
                            <TextInput
                                style={styles.searcField}
                                placeholder="Тег для пошуку"
                                onChangeText={(text) => this.validateInput(text)}/>
                        </View>
                        <TouchableOpacity onPress={() => this.setModalVisability(true)}>
                            <Image source={require('../img/controls.png')} style={{width: 20, height: 20, marginTop: 10}}/>
                        </TouchableOpacity>
                    </View>
                    <Modal isVisible={this.state.modalVisible} onPress={() => this.setModalVisability(false)}>
                        <View style={styles.filterModal}>
                            <TouchableOpacity onPress={() => this.setModalVisability(false)}>
                                <Text>X</Text>
                            </TouchableOpacity>
                            <View style={styles.row}>
                                <View style={styles.column}>
                                    <Dropdown
                                        label='Order'
                                        baseColor="#c5c5c5"
                                        onChangeText={(text, index, data) => this.setState({order: data[index].value})}
                                        data={order}/>
                                </View>
                                <View style={styles.column}>
                                    <Dropdown
                                        label='Sort'
                                        baseColor="#c5c5c5"
                                        onChangeText={(text, index, data) => this.setState({sort: data[index].value})}
                                        data={sort}/>
                                </View>
                            </View>
                            <View style={styles.row}>
                                <View style={styles.column}>
                                    <DatePicker
                                        date={this.state.fromdate}
                                        mode="date"
                                        placeholder="Fromdate"
                                        format="YYYY-MM-DD"
                                        confirmBtnText="Підтвердити"
                                        cancelBtnText="Скасувати"
                                        onDateChange={(date) => this.setState({fromdate : date})}/>
                                </View>
                                <View style={styles.column}>
                                    <DatePicker
                                        date={this.state.todate}
                                        mode="date"
                                        placeholder="Todate"
                                        format="YYYY-MM-DD"
                                        confirmBtnText="Підтвердити"
                                        cancelBtnText="Скасувати"
                                        onDateChange={(date) => this.setState({todate : date})}/>
                                </View>
                            </View>
                            <View>
                                <TouchableOpacity onPress={() => this.search()} style={styles.searchBTN}>
                                    <Text style={styles.searchBTNText}>Пошук</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Modal>
                    <InfiniteScroll
                        horizontal={false}
                        onLoadMoreAsync={this.loadMorePage}
                        distanceFromEnd={10}>
                            {this.props.postsList.map((data, index) =>
                                <TouchableOpacity onPress={() => this.route(data.question_id)} style={styles.singleCard} key={index}>
                                    <View style={styles.userNameImg}>
                                        <View style={styles.userImg}>
                                            <Image
                                                style={{width: 50, height: 50}}
                                                source={{uri: data.owner.profile_image}}/>
                                        </View>
                                        <View>
                                            <View style={styles.nameReputation}>
                                                <Text style={styles.name}>{data.owner.display_name}</Text>
                                                <Text style={styles.reputationDate}>{data.owner.reputation}</Text>
                                            </View>
                                            <Moment unix style={styles.reputationDate} element={Text} format="MMMM Do YYYY, h:mm:ss">{data.creation_date}</Moment>
                                        </View>
                                    </View>
                                    <Text style={styles.cardTitle}>{data.title}</Text>
                                    <View style={styles.tagsList}>
                                        {data.tags.map((tag, index) =>
                                            <View style={styles.singleTag} key={index}>
                                                <Text style={{color: '#ffffff'}}>{tag}</Text>
                                            </View>
                                        )}

                                    </View>
                                    <View style={styles.cardFooter}>
                                        <View style={styles.inline}>
                                            <Text style={styles.votesViews}>{data.score}</Text>
                                            <Text style={styles.votesViewsText}>votes</Text>
                                        </View>
                                        <View style={styles.answersCountBG}>
                                            <Text style={styles.answerCount}>{data.answer_count}</Text>
                                            <Text style={styles.answerText}>answers</Text>
                                        </View>
                                        <View style={styles.inline}>
                                            <Text style={styles.votesViews}>{data.view_count}</Text>
                                            <Text style={styles.votesViewsText}>views</Text>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            )}
                            {this.state.loadingData && (
                                <Text style={styles.preloadData}>Завантажую більше даних...</Text>
                            )}
                    </InfiniteScroll>
                </View>
            );
        }else{
            return (
                <View style={styles.container}>
                    <Text style={styles.loading}>Завантаження данних...</Text>
                </View>
            )
        }
    }
}

const mapStateToProps = state => {
    return{
        postsList: state.search.postsList
    }
}

export default connect(mapStateToProps, {postsChanged, clearStore})(App)
