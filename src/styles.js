import { StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    container:{
        backgroundColor: '#261f40',
        height: '100%',
        padding: 15
    },
    containerX:{
        backgroundColor: '#261f40',
        height: '100%',
        padding: 15,
        paddingTop: 35
    },
    search:{
        borderRadius: 3,
        backgroundColor: '#eb9c1b',
        padding: 10,
        flexDirection: 'row'
    },
    fieldHere:{
        position: 'relative',
        flexGrow: 1,
        marginRight: 15
    },
    searchButton:{
        position: 'absolute',
        alignItems: 'center',
        width: 40,
        height: 40,
        zIndex: 4
    },
    searcField:{
        height: 40,
        backgroundColor: '#ffffff',
        borderRadius: 20,
        paddingLeft: 50
    },
    showSettings:{
        width: 40,
        height: 40,
        alignItems: 'center'
    },
    singlePage:{
        backgroundColor: '#ffffff',
        borderRadius: 15,
        flexGrow: 1,
        overflow: 'hidden'
    },
    loading:{
        fontSize: 20,
        color: '#ffffff',
        textAlign: 'center',
        marginTop: 120
    },
    preloadData:{
        fontSize: 20,
        color: '#ffffff',
        textAlign: 'center'
    },
    singleCard:{
        backgroundColor: '#ffffff',
        borderRadius: 15,
        padding: 10,
        marginTop: 10,
        marginBottom: 10
    },
    userNameImg:{
        flexDirection: 'row'
    },
    userImg:{
        width: 50,
        height: 50,
        borderRadius: 25,
        overflow: 'hidden',
        marginRight: 5
    },
    fillSpace:{
        flexGrow: 1
    },
    nameReputation:{
        flexDirection: 'row',
        alignItems: 'baseline',
    },
    name:{
        fontSize: 20,
        color: '#000000',
        marginRight: 5
    },
    reputationDate:{
        fontSize: 15,
        color: '#c3c3c3'
    },
    userReputation:{
        borderWidth: 2,
        paddingTop: 3,
        paddingBottom: 3,
        paddingLeft: 5,
        paddingRight: 5,
        borderColor: '#54ae6e',
        borderRadius: 5,
        textAlign: 'center',
        marginLeft: 'auto'
    },
    userReputationText:{
        color: '#54ae6e',
        fontWeight: '700',
        fontSize: 18
    },
    cardTitle:{
        color: '#2480eb',
        fontSize: 20,
        marginTop: 10
    },
    tagsList:{
        flexDirection: 'row',
        paddingTop: 15,
        paddingBottom: 15,
        borderBottomWidth: 1,
        borderColor: '#c3c3c3',
        overflow: 'hidden'
    },
    singleTag:{
        borderRadius: 3,
        backgroundColor: '#ee0046',
        marginRight: 5,
        padding: 5
    },
    cardFooter:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 15
    },
    inline:{
        flexDirection: 'row',
        alignItems: 'baseline'
    },
    votesViews:{
        fontSize: 20,
        color: '#000000',
        marginRight: 5
    },
    votesViewsText:{
        fontSize: 15,
        color: '#c3c3c3'
    },
    answersCountBG:{
        flexDirection: 'row',
        alignItems: 'baseline',
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 10,
        paddingRight: 15,
        borderRadius: 5,
        backgroundColor: '#54ae6e'
    },
    answerCount:{
        color: '#ffffff',
        fontSize: 18,
        marginRight: 5
    },
    answerText:{
        color: '#ffffff',
        fontSize: 14
    },
    filterModal:{
        backgroundColor: '#ffffff',
        height: 200,
        padding: 15
    },
    formInput:{
        width: 250,
        height: 40,
        borderBottomWidth: 1,
        borderColor: '#f60',
        backgroundColor: '#ffffff'
    },
    row:{
        flexDirection: 'row'
    },
    column:{
        width: '50%'
    },
    searchBTN:{
        width: 200,
        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: 20,
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 10,
        paddingRight: 15,
        borderRadius: 5,
        backgroundColor: '#54ae6e'
    },
    searchBTNText:{
        color: '#ffffff',
        textAlign: 'center',
        fontSize: 18
    },
    goBack:{
        margin: 15
    },
    goBackText:{
        fontSize: 18,
        color: '#54ae6e'
    }
})

export default styles;
