import { POSTS_CHANGE, CLEAR_STORE } from '../types';

export const postsChanged = (text) => {
    return {
        type: POSTS_CHANGE,
        payload: text
    }
}

export const clearStore = () => {
    return {
        type: CLEAR_STORE,
        payload: []
    }
}
