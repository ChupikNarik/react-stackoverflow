const constants = {
    API: 'https://api.stackexchange.com/2.2/questions',
    DEFAULT_PARAMETRS: '?site=stackoverflow&pagesize=20'
}

export default constants;
