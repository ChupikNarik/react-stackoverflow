import React from 'react';

import { createStackNavigator } from "react-navigation";
import Main from '../pages';
import viewPage from '../pages/viewPage';



const AppNavigator = createStackNavigator(
    {
        Main: { screen: Main },
        viewPage: { screen: viewPage }
    },
    {
        initialRouteName: 'Main'
    }
);

export default AppNavigator;
